package bleble103.mplayer;

public enum Inputs {
    ARROW_UP,
    ARROW_DOWN,
    ENTER,
    QUIT,
    PREVIOUS,
    NEXT,
    STOP,
    DEFAULT
}
