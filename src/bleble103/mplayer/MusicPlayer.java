package bleble103.mplayer;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MusicPlayer {
    private Player player = null;
    private ExecutorService executor;
    private final MyConsole console;
    private File lastPlayed = null;
    private boolean stopped = false;
    int frames = 0;


    public void shutdown() throws IOException{
        if(player != null) {
            stopped = true;
            player.close();
            console.printMessage("Stopped "+lastPlayed.getName());
        }
    }

    public void stop() throws IOException{
        if(player != null) {
            stopped = true;
            player.close();
            console.printMessage("Stopped "+lastPlayed.getName());
        }
        else console.printError("Nothing is playing");
        player = null;
    }

    public MusicPlayer(MyConsole console){
        this.console = console;
        this.executor = Executors.newSingleThreadExecutor();
    }

    public void play(File file) throws JavaLayerException, IOException {
        if(player == null) player = new Player(new FileInputStream(file));
        else{
            console.printMessage("Stopped "+lastPlayed.getName());
            stopped = true;
            player.close();
            player = new Player(new FileInputStream(file));
        }
        final Player finalPlayer = player;
        console.printMessage("Playing "+file.getName());
        lastPlayed = file;
        executor = Executors.newSingleThreadExecutor();
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    finalPlayer.play();
                    if(!stopped) console.endOfSong();
                }
                catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }
        });
        stopped = false;
        executor.shutdown();
    }
}
