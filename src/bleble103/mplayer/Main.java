package bleble103.mplayer;

import jline.console.ConsoleReader;

import java.io.IOException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodType;
import java.lang.reflect.Constructor;

public class Main {
    private static ConsoleReader console;

    public static void main(String[] args){
        try{
            console = new ConsoleReader();
            MyConsole myConsole = new MyConsole(console);
        }catch (IOException e){
            System.out.println("Cannot create console: "+e.getMessage());
        }
    }
}
