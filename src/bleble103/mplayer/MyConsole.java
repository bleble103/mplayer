package bleble103.mplayer;

import javazoom.jl.decoder.JavaLayerException;
import jline.console.ConsoleReader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class MyConsole {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";
    private Map<Integer, File> files;
    private List<File> playbackFiles;
    private FileNavigator navigator;
    private MusicPlayer musicPlayer;
    private String[] lines = new String[16];
    private String messageLine = "";
    private ConsoleReader console;
    private int cursor = 0;
    private int firstElement;
    private int lastElement;
    private Selection selection;
    private File currentlyPlaying=null;
    private boolean loop = true;

    private void stopSong() throws IOException{
        musicPlayer.stop();
        currentlyPlaying = null;
        playbackFiles = null;
        printContents();
    }

    public void endOfSong() throws IOException{
        if(currentlyPlaying.equals(playbackFiles.get(playbackFiles.size()-1))){
            stopSong();
        }
        else{
            playNext();
        }
    }

    public void playNext() throws IOException {
        if(playbackFiles == null) return;
        Iterator<File> iterator = playbackFiles.iterator();

        if(!iterator.hasNext()) return;

        //find current song
        while(iterator.next() != currentlyPlaying);
        //iterator.remove();

        if(iterator.hasNext()){
            currentlyPlaying = iterator.next();
            try {
                musicPlayer.play(currentlyPlaying);
            }catch (JavaLayerException e){
                printError(e.getMessage());
            }
        }else{
            printError("No more songs");
        }
    }

    public void playPrevious() throws IOException{
        if(playbackFiles == null) return;

        Iterator<File> iterator = playbackFiles.iterator();
        if(!iterator.hasNext()) return;
        File previous = null;
        File current = iterator.next();

        while(current != currentlyPlaying && iterator.hasNext()){
            previous = current;
            current = iterator.next();
        }

        if(previous != null){
            currentlyPlaying = previous;
            try {
                musicPlayer.play(currentlyPlaying);
            }catch (JavaLayerException e){
                printError(e.getMessage());
            }
        }else{
            printError("No more songs");
        }

    }

    private void copyCurrentlyMusicFiles(){
        playbackFiles = new LinkedList<>();
        for(File file:files.values()){
            if(file.isFile()){
                playbackFiles.add(file);
            }
        }
    }

    private Inputs getInput() throws IOException{
        int input = console.readCharacter();
//        console.println(input+"");
//        console.flush();
//        input = console.readCharacter();
        if(input == 27){
            //ARROWS
            console.readCharacter();
            input = console.readCharacter();
            if(input == 65){
                return Inputs.ARROW_UP;
            }else if(input == 66){
                return Inputs.ARROW_DOWN;
            }
        }
        else if(input == 13){
            return Inputs.ENTER;

        }
        else if(input == 113){
            return Inputs.QUIT;
        }
        else if(input == 115){
            return Inputs.STOP;
        }
        else if(input == 44){
            return Inputs.PREVIOUS;
        }
        else if(input == 46){
            return Inputs.NEXT;
        }
        return Inputs.DEFAULT;
    }

    private void incrementCursor() throws IOException{
        if(cursor + 1 < files.size()) cursor++;
        if(cursor>lastElement){
            firstElement++;
            lastElement++;
        }
        printContents();
    }

    private void decrementCursor() throws IOException{
        if(cursor - 1 >= 0) cursor--;
        if(cursor < firstElement){
            firstElement--;
            lastElement--;
        }
        printContents();
    }

    private void printInColor(String color, String content) throws IOException{
        messageLine = color;
        messageLine += content;
        messageLine += ANSI_RESET;
        printContents();
        //messageLine = "";
    }

    public void printMessage(String message) throws IOException{
        printInColor(ANSI_YELLOW, message);
    }
    public void printError(String error) throws IOException{
        printInColor(ANSI_RED, error);
    }

    private void printLines() throws IOException{
        console.clearScreen();
        for(String line:lines){
            console.println(line);
        }
        console.println(messageLine);
        console.flush();
    }

    private void printContents() throws IOException{

        for(int i=0; i<lines.length; i++)lines[i] = "";

        int lineCounter = 0;

        for (Map.Entry<Integer, File> file : files.entrySet()) {
            if(file.getKey() < firstElement || file.getKey() > lastElement) continue;
            lines[lineCounter] += String.format("%7d",file.getKey())+" "+file.getValue().getName();
            if(file.getValue().isDirectory()){
                lines[lineCounter] = " * " + lines[lineCounter].substring(3);
            }

            if(file.getKey() == cursor) {
                String selected = "(+)";
                this.selection = Selection.DIRECTORY;
                if(file.getValue().isFile()) {
                    this.selection = Selection.FILE;
                    selected = "(>)";
                }
                lines[lineCounter] = selected + lines[lineCounter].substring(3);
            }
            if(currentlyPlaying != null && file.getValue().getName().equals(currentlyPlaying.getName())){
                lines[lineCounter] = ANSI_GREEN + lines[lineCounter] + ANSI_RESET;
            }
            lineCounter++;
        }
        printLines();
    }

    private void listContents() throws IOException{
        cursor = 0;
        files = navigator.listFiles();
        firstElement = 0;
        lastElement = Math.min(files.size()-1, lines.length-1);
        if(files.size() > 1) cursor = 1;
        printContents();

    }
    public MyConsole(ConsoleReader console) throws IOException {
        this.console = console;
        navigator = new FileNavigator();
        musicPlayer = new MusicPlayer(this);

        listContents();

        while(loop) {
            switch (getInput()) {
                case ARROW_UP:
                    decrementCursor();
                    break;
                case ARROW_DOWN:
                    incrementCursor();
                    break;
                case ENTER:
                    if (selection == Selection.DIRECTORY) {
                        navigator.changeDir(files.get(cursor));
                        listContents();
                    } else if (selection == Selection.FILE) {
                        try {
                            currentlyPlaying = files.get(cursor);
                            copyCurrentlyMusicFiles();
                            musicPlayer.play(files.get(cursor));
                        } catch (Exception e) {
                            System.out.println("Problem playing song");
                        }
                    }
                    break;
                case NEXT:
                    playNext();
                    break;
                case PREVIOUS:
                    playPrevious();
                    break;
                case STOP:
                    stopSong();
                    break;
                case QUIT:
                    musicPlayer.shutdown();
                    loop = false;
                    break;
            }


        }
    }
}
