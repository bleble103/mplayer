package bleble103.mplayer;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FileNavigator {
    protected File currentDir;
    protected Map<Integer, File> currentLs;
    public FileNavigator(){
        currentDir = new File(System.getProperty("user.home"));
    }

    public boolean isMusicOrDir(File file){
        if(file == null) return false;
        if(file.isDirectory()) return true;
        String[] fileSplit = file.getName().split("\\.");
        if(fileSplit.length == 0) return false;
        return fileSplit[fileSplit.length-1].equals("mp3");
    }

    public void changeDir(File directory){
        currentDir = directory;
    }

    public Map<Integer, File> listFiles(){
        currentLs = new HashMap<>();
        currentLs.put(0, new File(currentDir,".."));
        int indexer = 0;
        File[] dirFiles = currentDir.listFiles();
        for(File file: dirFiles){
            if(file.isHidden()) continue;
            if(file.isDirectory()){
                currentLs.put(++indexer, file);
            }
        }
        for(File file: dirFiles){
            if(file.isFile()){
                if(isMusicOrDir(file)) currentLs.put(++indexer, file);
            }
        }
        return currentLs;
    }
}
